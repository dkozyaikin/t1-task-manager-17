package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.api.service.ITaskService;
import ru.t1.dkozyaikin.tm.enumerated.Sort;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dkozyaikin.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.IdEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozyaikin.tm.exception.field.NameEmptyException;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task add(Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById((id));
    }

    @Override
    public Task findOneByIndex(final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (index == null || index < 0) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > taskRepository.getSize()) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
