package ru.t1.dkozyaikin.tm.exception.field;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public class AbstractFieldException extends AbstractException {

    public AbstractFieldException(String message) {
        super(message);
    }

}
