package ru.t1.dkozyaikin.tm.exception.system;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public class AbstractSystemException extends AbstractException {

    public AbstractSystemException(String message) {
        super(message);
    }

}
