package ru.t1.dkozyaikin.tm.exception.entity;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException(String message) {
        super(message);
    }

}
