package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.exception.AbstractException;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command) throws AbstractException;

    AbstractCommand getCommandByName(String name) throws AbstractException;

    AbstractCommand getCommandByArgument(String argument) throws AbstractException;

    Collection<AbstractCommand> getTerminalCommands();

    void clearOutput();

}
