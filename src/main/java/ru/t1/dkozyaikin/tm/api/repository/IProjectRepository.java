package ru.t1.dkozyaikin.tm.api.repository;

import ru.t1.dkozyaikin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project add(Project project);

    void clear();

    Integer getSize();

    boolean existById(String id);

}
