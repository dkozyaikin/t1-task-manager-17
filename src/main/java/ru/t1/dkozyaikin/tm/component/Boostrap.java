package ru.t1.dkozyaikin.tm.component;

import ru.t1.dkozyaikin.tm.api.repository.ICommandRepository;
import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.api.service.*;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.command.project.*;
import ru.t1.dkozyaikin.tm.command.system.*;
import ru.t1.dkozyaikin.tm.command.task.*;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkozyaikin.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.model.Task;
import ru.t1.dkozyaikin.tm.repository.CommandRepository;
import ru.t1.dkozyaikin.tm.repository.ProjectRepository;
import ru.t1.dkozyaikin.tm.repository.TaskRepository;
import ru.t1.dkozyaikin.tm.service.*;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class Boostrap implements IServiceLocator {

    private static final String VERSION = "1.17.0";

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        try {
            registry(new ApplicationAboutCommand());
            registry(new ApplicationHelpCommand());
            registry(new ApplicationVersionCommand());
            registry(new CommandListCommand());
            registry(new ArgumentListCommand());
            registry(new ApplicationExitCommand());

            registry(new ProjectCreateCommand());
            registry(new ProjectListCommand());
            registry(new ProjectShowByIdCommand());
            registry(new ProjectShowByIndexCommand());
            registry(new ProjectUpdateByIdCommand());
            registry(new ProjectUpdateByIndexCommand());
            registry(new ProjectRemoveByIdCommand());
            registry(new ProjectRemoveByIndexCommand());
            registry(new ProjectClearCommand());
            registry(new ProjectStartByIdCommand());
            registry(new ProjectStartByIndexCommand());
            registry(new ProjectCompleteByIdCommand());
            registry(new ProjectCompleteByIndexCommand());
            registry(new ProjectChangeStatusByIdCommand());
            registry(new ProjectChangeStatusByIndexCommand());

            registry(new TaskCreateCommand());
            registry(new TaskListCommand());
            registry(new TaskClearCommand());
            registry(new TaskShowByIdCommand());
            registry(new TaskShowByIndexCommand());
            registry(new TaskUpdateByIdCommand());
            registry(new TaskUpdateByIndexCommand());
            registry(new TaskRemoveByIdCommand());
            registry(new TaskRemoveByIndexCommand());
            registry(new TaskStartByIdCommand());
            registry(new TaskStartByIndexCommand());
            registry(new TaskCompleteByIdCommand());
            registry(new TaskCompleteByIndexCommand());
            registry(new TaskChangeStatusByIdCommand());
            registry(new TaskChangeStatusByIndexCommand());
            registry(new TaskBindToProjectCommand());
            registry(new TaskUnbindFromProjectCommand());
        }
        catch (AbstractException e) {
            getLoggerService().error(e);
        }
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    private static void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        projectRepository.add(new Project("P4", "P1D", Status.IN_PROGRESS));
        projectRepository.add(new Project("P2", "P2D", Status.NOT_STARTED));
        projectRepository.add(new Project("P3", "P3D", Status.COMPLETED));
        projectRepository.add(new Project("P1", "P4D", Status.IN_PROGRESS));
        taskRepository.add(new Task("C TASK", "T1D"));
        taskRepository.add(new Task("A TASK", "T2D"));
        taskRepository.add(new Task("D TASK", "T3D"));
        taskRepository.add(new Task("B TASK", "T4D"));
    }

    private void initLogger() {
        loggerService.info("Task Manager started");
        Runtime.getRuntime().addShutdownHook(new Thread() {
           public void run() {
               loggerService.info("Task Manager closed");
           }
        });
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) {
        if (processArgument(args)) exit();
        initDemoData();
        initLogger();

        processCommands();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[DONE]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processCommand(final String command) throws CommandNotSupportedException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    private void processArgument(final String argument) throws ArgumentNotSupportedException {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private boolean processArgument(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
